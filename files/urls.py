from django.urls import path

from . import views

urlpatterns = [
    path('', views.list_tribes, name='list_tribes'),
    path('tribe_files/<int:tribe_id>', views.list_tribe_files, name='list_tribe_files'),
    path('tribe_files/<int:tribe_id>/upload', views.upload_file, name='upload_file'),
    path('tribe_files/<int:tribe_id>/download/<int:file_id>', views.download_file, name='download_file'),
    path('tribe_files/<int:tribe_id>/remove/<int:file_id>', views.remove_file, name='remove_file'),
]