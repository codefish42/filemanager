from django import forms

class UploadedFileForm(forms.Form):
   description = forms.CharField(label='Description')
   file = forms.FileField(label='Uploaded file')