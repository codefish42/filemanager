from django.db import models
from django.conf import settings
from django.contrib.auth.models import AbstractUser

import os

from filemanager.settings import UPLOADED_FILE_DIRECTORY

class Tribe(models.Model):
    name = models.CharField(max_length=64)
    users = models.ManyToManyField(settings.AUTH_USER_MODEL)
    
    def __str__(self):
        return self.name


class UploadedFile(models.Model):
    name = models.CharField(verbose_name="Name of the file", blank=False, max_length=256)
    mime_type = models.CharField(verbose_name="MIME type", max_length=128)
    fingerprint = models.BinaryField(verbose_name="SHA-256 fingerprint of the file", max_length=32)
    tribe = models.ForeignKey(Tribe, verbose_name="Tribe that owns the file", blank=True, null=True, on_delete=models.SET_NULL)
    submitter = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name="User that have submitted the file", blank=True, null=True, on_delete=models.SET_NULL)
    timestamp = models.DateTimeField(auto_now_add=True)
    description = models.TextField(verbose_name="Description of the file")

    @property
    def hexfingerprint(self):
        return self.fingerprint.hex()

    @property
    def filepath(self):
        return os.path.join(UPLOADED_FILE_DIRECTORY, self.hexfingerprint)

    @property
    def size(self):
        return os.path.getsize(self.filepath)
