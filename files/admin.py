from django.contrib import admin
from .models import Tribe, UploadedFile

admin.site.register(Tribe)
admin.site.register(UploadedFile)
