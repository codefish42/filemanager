from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.template import loader


from django.http import HttpResponse, HttpResponseForbidden, HttpResponseNotFound, HttpResponseBadRequest, FileResponse, StreamingHttpResponse
from django.views.decorators.http import require_POST, require_http_methods

from .models import UploadedFile, Tribe
from .forms import UploadedFileForm
from .utils import FileManager

import os

@login_required
def list_tribes(request):
    user_tribes = request.user.tribe_set.all()
    template = loader.get_template('files/tribelist.html')
    context = {"user_tribes": user_tribes, "user":request.user}
    return HttpResponse(template.render(context, request))

def check_tribe(request, tribe_id):
    try:
        tribe = Tribe.objects.get(pk=tribe_id)
    except:
        return None
    if request.user not in tribe.users.all():
        return False
    else:
        return tribe

@login_required
def list_tribe_files(request, tribe_id, uploaded_file=False):
    tribe = check_tribe(request, tribe_id)
    if tribe is None:
        return HttpResponseNotFound("Inexistent tribe")
    elif tribe is False:
        return HttpResponseForbidden("You are not in this tribe")
    else:
        tribe_files = tribe.uploadedfile_set.all()
        template = loader.get_template('files/tribefilelist.html')
        upload_form = UploadedFileForm()
        context = {"tribe": tribe, "tribe_files": tribe_files, "user":request.user, "uploaded_file": uploaded_file, "upload_form": upload_form}
        return HttpResponse(template.render(context, request))

@login_required
@require_POST
def upload_file(request, tribe_id):
    # create a form instance and populate it with data from the request:
    form = UploadedFileForm(request.POST, request.FILES)
    if form.is_valid():
        tribe = check_tribe(request, tribe_id)
        if tribe is None:
            return HttpResponseNotFound("Inexistent tribe")
        elif tribe is False:
            return HttpResponseForbidden("You are not in this tribe")
        else:
            print("Valid form")
            description = form['description'].value()
            file_manager = FileManager(request.FILES['file'])
            file_manager.treat()
            obj = UploadedFile(name=file_manager.name, mime_type=file_manager.mime_type, fingerprint=file_manager.fingerprint, tribe=tribe, submitter=request.user, description=description)
            obj.save()
            return list_tribe_files(request, tribe_id, uploaded_file=True)
    else:
        return HttpResponseBadRequest("Should be a POST request and a valid form, errors: {}".format(form.errors))

@login_required
def download_file(request, tribe_id: int, file_id: int):
    tribe = check_tribe(request, tribe_id)
    if tribe is None:
        return HttpResponseNotFound("Inexistent tribe")
    elif tribe is False:
        return HttpResponseForbidden("You are not in this tribe")
    else:
        try:
            file = UploadedFile.objects.get(pk=file_id)
        except UploadedFile.DoesNotExist as e:
            return HttpResponseNotFound("Inexistent file")
        if file.tribe.id != tribe.id:
            return HttpResponseForbidden("The requested file is not in the specified tribe")
        else:
            f = open(file.filepath, 'rb')
            r = StreamingHttpResponse(iter(lambda: f.read(4096), b''))
            r._resource_closers.append(f.close)
            r['Content-Type'] = file.mime_type
            return r

@login_required
@require_http_methods(["DELETE"])
def remove_file(request, tribe_id: int, file_id: int):
    tribe = check_tribe(request, tribe_id)
    if tribe is None:
        return HttpResponseNotFound("Inexistent tribe")
    elif tribe is False:
        return HttpResponseForbidden("You are not in this tribe")
    else:
        try:
            file = UploadedFile.objects.get(pk=file_id)
        except UploadedFile.DoesNotExist as e:
            return HttpResponseNotFound("Inexistent file")
        if file.tribe.id != tribe.id:
            return HttpResponseForbidden("The requested file is not in the specified tribe")
        else:
            fingerprint = file.fingerprint
            filepath = file.filepath
            file.delete()
            remaining = len(UploadedFile.objects.filter(fingerprint=fingerprint))
            if remaining == 0:
                # we delete the file from the disk since there is no more record linking to it
                os.remove(filepath)
            return HttpResponse("File removed")