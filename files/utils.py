from filemanager.settings import UPLOADED_FILE_DIRECTORY

import tempfile
import hashlib
import shutil
import os

BUFFER_LEN = 2048

class FileManager(object):
    def __init__(self, file_handler):
        self.file_handler = file_handler
        self.fingerprint = None
        self.mime_type = file_handler.content_type
        self.name = file_handler.name

    def treat(self):
        # f is a temporary file
        h = hashlib.new("sha256")
        with tempfile.NamedTemporaryFile(mode="wb", prefix="django-uploaded-file", suffix=".tmp", delete=False) as f:
            tmpfile_name = f.name
            for chunk in self.file_handler.chunks():
                f.write(chunk)
                h.update(chunk)
            self.fingerprint = h.digest()
        shutil.move(tmpfile_name, os.path.join(UPLOADED_FILE_DIRECTORY, self.fingerprint.hex()))
        
